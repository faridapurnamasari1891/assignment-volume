const readline = require('readline')
const tes = readline.createInterface({
    input: process.stdin,
    output: process.stdout
})

const volRec=require('./RectangularPrism.js')
const volTube=require('./Tube.js')
const volCube=require('./cube.js')


function isEmptyOrSpaces(str){
    return str === null || str.match(/^ *$/) !== null;
}

function volsphere(radius){
  var volume=4/3*3.14*radius**3
  return volume
}
function inputRadius(){
  tes.question(`\nRadius: `, radius =>{
    if (!isNaN(radius) && !isEmptyOrSpaces(radius)){
      console.log(`Sphere Volume: ${volsphere(radius)}`)
      inputOption()            }
  else {
    console.log(`Radius must be a number!`)
    inputRadius()
  }
  })
}



 function inputOption(){
 console.log("\nHello, pick one shape volume that you want to calculate")
 console.log("1. Rectangular Prism")
 console.log("2. Tube")
 console.log("3. Cube")
 console.log("4. Sphere")
 console.log("5. Exit")

 tes.question(`Your pick: `, pick => {
     if (pick==1){
    volRec.inputLength()
      } else if (pick==2) {
        volTube.inputHeight()
  
      }
        else if (pick==3){
          volCube.inputLength()

      }
        else if (pick==4){
        inputRadius()
      }
      else if (pick==5){
      tes.close();
    }
      else if(isNaN(pick) || isEmptyOrSpaces(pick)) {
        console.log(`\nIt is not a number!\n`)
        inputOption()
      }else {
        console.log(`\nNumber is not valid!\n`)
        inputOption()
 }
 })
 }



 inputOption()

 module.exports.rl = tes
 module.exports.isEmptyOrSpaces = isEmptyOrSpaces
 module.exports.inputOption=inputOption
